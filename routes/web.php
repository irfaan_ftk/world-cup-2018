<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/', 'HomeController@index')->name('home');
Route::get('/place_score/{id}', 'HomeController@place_score')->name('place_score');
Route::post('/submit_score', 'HomeController@submit_score')->name('submit_score');
Route::get('/pronostics', 'HomeController@pronostics')->name('pronostics');
Route::get('/matches', 'HomeController@matches')->name('matches');


Route::get('/update', 'HomeController@update')->name('update');