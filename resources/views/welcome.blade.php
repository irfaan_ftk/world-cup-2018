<!doctype html>
<html lang="en">
<!--

Page    : index / MobApp
Version : 1.0
Author  : Colorlib
URI     : https://colorlib.com

 -->
<style>
 body { 
    background-image: url('/img/intro-bg.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center; 
    background-size: cover;
}
.square{
    border-radius: 10px;
    background-color: #ffffff8a;
    padding: 10px;
    color: black;
    margin-top:10px;
}
.col-sm-6{
    pading:30px
}
h1 {
    font-size: 30px!important;
    font-weight: 300;
    letter-spacing: -1px;
    margin-bottom: 1.5rem;
}
.table td,th{
    border-top: 1px solid #4c070730!important;
}
.img-responsive {
    max-width: 100%;
    height: auto;
}

</style>
<head>
    <title>World Cup 2018</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Mobland - Mobile App Landing Page Template">
    <meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive">

    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Themify Icons -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- Main css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">

    <!-- Nav Menu -->

    <div class="nav-menu fixed-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-dark navbar-expand-lg">
                        <a class="navbar-brand" href="index.html">World Cup 2018</a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                        <div class="collapse navbar-collapse" id="navbar">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item"> <a class="nav-link active" href="#home">HOME <span class="sr-only">(current)</span></a> </li>
                               
                                <li class="nav-item"> <a class="nav-link" href="{{route('pronostics')}}">PRONOSTICS</a> </li>
                                 <li class="nav-item"> <a class="nav-link" href="{{route('matches')}}">MATCHES</a> </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <div style="margin-top:100px;" class="container" id="gallery">
      




<div class="container-fluid">
	<div class="row">
		
		<div class="col-md-6">
         <center>
          <div class="square">
         <h1>Leaderboard</h1>
        <table class="table">
        <tr><th>Rank</th><th>Name</th><th>Points</th></tr>
        <?php
        $count  = 1;
        ?>
        @foreach($leaderboards as $leaderboard)
        <tr><td>{{$count}}</td><td>{{$leaderboard->name}}</td><td>{{$leaderboard->pts}}</td></tr>
        <?php
        $count++;
        ?>
        @endforeach
        </table>
         </div>
        </center>
		</div>
        <div class="col-md-6">
        <center>
        <div class="square">
         


           <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h1>Matches of the day</h1></div>

            <!-- Table -->
            <table class="table">
             @foreach($matches as $match)
             <?php
            
             $team1 = App\Team::where('team_name','like',$match->team1)->first();
             $team1_flag = $team1->flag;
             $team2 = App\Team::where('team_name','like',$match->team2)->first();
             $team2_flag = $team2->flag;
             ?>
             
            <tr>
                <td>
                {{$match->team1}}
                </td>
                <td>
                <img style="margin-left:10px;margin-right:10px;" width="25" src="{{$team1_flag}}">
                </td>
                 <td>
                 vs
                </td>
                <td>
                 <img style="margin-left:10px;margin-right:10px;" width="25"  src="{{$team2_flag}}">
                </td>
                <td>
                {{$match->team2}}
                </td>
                <td>
                <button type="button" class="btn btn-success" onclick="window.location.href='{{route('place_score', $match->id)}}'">Play</button>
                </td>
            </tr>
            
            
             @endforeach
            </table>
            </div>




        </div>
        </center>
		</div>
	</div>
</div>




<center>

</center>

    </div>
   
    <!-- // end .section -->

<div class="container" style="margin-top:50px;">
<img src="/img/groups.png" class="img img-responsive">
</div>
 
<img src="/img/Match-schedule.png" class="img img-responsive">

 


    



 

    
   
    <!-- jQuery and Bootstrap -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <!-- Plugins JS -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- Custom JS -->
    <script src="js/script.js"></script>

</body>

</html>
