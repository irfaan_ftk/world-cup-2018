<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Match;
use App\Pronostic;
use App\Player;
use App\Leaderboard;
use DB;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = date("Y-m-d H:i:s");
        $endday =  new DateTime('NOW');
        $endday->setTime(23, 59);
     
        $matches = Match::where('date','>',$date)->where('date','<',$endday)->orderBy('date')->get();
        $leaderboards = Leaderboard::join('players','players.id','=','leaderboard.player_id')->orderBy('pts', 'desc')->get();
        
       
       
       return view('welcome',compact('matches','leaderboards'));
    }

    public function place_score($id){
         $match = Match::find($id);
         $players = Player::all();
         return view('place_score',compact('match','players'));
    }

     public function submit_score(Request $request){
         $match_id = $request->input('match_id');
         $score1 = $request->input('score1');
         $score2 = $request->input('score2');
         $player = $request->input('player_id');

         $pronostic = new Pronostic;
         $pronostic->match_id =  $match_id;
         $pronostic->pscore1 = $score1;
         $pronostic->pscore2 = $score2;
         $pronostic->player_id = $player;
         $pronostic->save();
    
        $success_title  = "Your score has been placed!";
        $success_message = "Good Luck!";
       
        return view('success', compact('success_title', 'success_message'));
    }

    public function pronostics(){
        $pronostics = Pronostic::join('players','players.id','=','pronostics.player_id')
        ->join('matches','matches.id','=','pronostics.match_id')
        ->get();
        
        return view('pronostics',compact('pronostics'));
    }

    public function update(){
        //select * from players inner join pronostics on pronostics.player_id = players.id inner join matches on matches.id = pronostics.match_id where pronostics.pscore1 = matches.score1 and pronostics.pscore2 = matches.score2 

            $leaderboards = Leaderboard::all();
            foreach($leaderboards as $leaderboard){
                $leaderboard->pts = 0;
                $leaderboard->save();
            }
       
    }

    public function matches(){
        $matches =  Match::orderBy('date')->get();
         return view('matches',compact('matches'));
    }

    
}
